package com.eli.Mongo;

import com.eli.Mongo.Gui.VentanaController;
import com.eli.Mongo.Gui.VentanaModel;
import com.eli.Mongo.Window.Ventana;

/**
 * Created by Eliseo on 06/03/2016.
 */
public class Main {
    public static void main (String[] args) {
        Ventana ventana=new Ventana();
        VentanaModel ventanaModel=new VentanaModel();
        VentanaController ventanaController=new VentanaController(ventana,ventanaModel);
    }



}
