package com.eli.Mongo.Base;



import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by Eliseo on 05/03/2016.
 */
public class Cliente {
    private ObjectId id;
    private String nombre;
    private int edad;
    private Date fecha_nacimiento;
    private Entrada entrada;
    public static final String COLECCION = "Cliente";

    public Entrada getEntrada() {
        return entrada;
    }

    public void setEntrada(Entrada entrada) {
        this.entrada = entrada;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public static String getCOLECCION() {
        return COLECCION;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }



    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    @Override
    public String toString() {
        return nombre+ edad;
    }
}
