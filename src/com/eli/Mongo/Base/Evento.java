package com.eli.Mongo.Base;



import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by Eliseo on 05/03/2016.
 */
public class Evento {

    private ObjectId id;
    private String nombre;
    private String ciudad;
    private int numero;
    private int precio_construccion;
    private Cliente cliente;
    private DJ dj;

    public static final String COLECCION = "Evento";
    private Date fecha;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public DJ getDj() {
        return dj;
    }

    public void setDj(DJ dj) {
        this.dj = dj;
    }

    public ObjectId getId() {
        return id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public static String getCOLECCION() {
        return COLECCION;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPrecio_construccion() {
        return precio_construccion;
    }

    public void setPrecio_construccion(int precio_construccion) {
        this.precio_construccion = precio_construccion;
    }

    @Override
    public String toString() {
        return nombre +"  "+ precio_construccion;
    }

    public void setId(ObjectId id) {
    }


}
