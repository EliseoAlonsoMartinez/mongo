package com.eli.Mongo.Base;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by Eliseo on 05/03/2016.
 */
public class DJ {
    private ObjectId id;
    private String nombre;
    private int edad;
    private int sueldo;
    private Date fecha_nacimiento_dj;
    private Evento evento;
    public static final String COLECCION = "DJ";


    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public static String getCOLECCION() {
        return COLECCION;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getSueldo() {
        return sueldo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    public Date getFecha_nacimiento_dj() {
        return fecha_nacimiento_dj;
    }

    public void setFecha_nacimiento_dj(Date fecha_nacimiento_dj) {
        this.fecha_nacimiento_dj = fecha_nacimiento_dj;
    }

    @Override
    public String toString() {
        return nombre +"  "+sueldo;
    }


}
