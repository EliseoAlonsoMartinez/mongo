package com.eli.Mongo.Base;


import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by Eliseo on 05/03/2016.
 */
public class Entrada {
    private ObjectId id;
    private String color;
    private int numero;
    private int precio;
    private Date fecha;
    private Evento evento;
    private Cliente cliente;
    public static final String COLECCION = "Entrada";

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public static String getCOLECCION() {
        return COLECCION;
    }

    @Override
    public String toString() {
        return color ;
    }
}
