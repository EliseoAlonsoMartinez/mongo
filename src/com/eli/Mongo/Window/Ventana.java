package com.eli.Mongo.Window;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * Created by Eliseo on 05/03/2016.
 */
public class Ventana {
    public JTabbedPane tabbedPane1;
    public JPanel panel1;
    public JTextField tfNombreDj;
    public JTextField tfEdadDj;
    public JTextField tfSueldoDj;
    public JButton btNuevoDj;
    public JButton btGuardarDj;
    public JButton btModificarDj;
    public JButton btEliminarDj;
    public JTextField tfColorEntrada;
    public JTextField tfNumeroEntrada;
    public JTextField tfPrecioEntrada;
    public JButton btNuevoEntrada;
    public JButton btGuardarEntrada;
    public JButton btModificarEntrada;
    public JButton btEliminarEntrada;
    public JDateChooser jdcFechaEntrada;
    public JDateChooser jdcFechaNacimientoDj;
    public JTextField tfNombreCliente;
    public JTextField tfEdadCLiente;
    public JButton btNuevoCliente;
    public JButton btGuardarCliente;
    public JButton btModificarCliente;
    public JButton btEliminarCliente;
    public JDateChooser jdcFechaNacimientoCliente;
    public JComboBox cbEventoEntrada;
    public JComboBox cbEventoDj;
    public JComboBox cbEntradaCliente;
    public JComboBox cbClienteEntrada;
    public JButton btNuevoEvento;
    public JButton btGuardarEvento;
    public JButton btModificarEvento;
    public JButton btEliminarEvento;
    public JTextField tfNombreEvento;
    public JComboBox cbClienteEvento;
    public JComboBox cbDjEvento;
    public JList lEvento;
    public JList lCliente;
    public JList lEntrada;
    public JList lDj;
    public JTextField tfCiudadEvento;
    public JTextField tfNumeroEvento;
    public JTextField tfPrecioConstruccionEvento;
    public JTextField tfBuscarDj;
    public JButton btBuscarDj;

    private DefaultListModel modeloListaCliente;
    private DefaultListModel modeloListaEvento;
    private DefaultListModel modeloListaEntrada;
    private DefaultListModel modeloListaDj;

    public Ventana(){
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("MONGODB");
        frame.pack();
        frame.setVisible(true);
        initModelos();
    }

    private void initModelos(){
        modeloListaCliente=new DefaultListModel();
        modeloListaDj=new DefaultListModel();
        modeloListaEntrada=new DefaultListModel();
        modeloListaEvento=new DefaultListModel();
    }

    public JButton getBtBuscarDj() {
        return btBuscarDj;
    }

    public void setBtBuscarDj(JButton btBuscarDj) {
        this.btBuscarDj = btBuscarDj;
    }

    public JTextField getTfBuscarDj() {
        return tfBuscarDj;
    }

    public void setTfBuscarDj(JTextField tfBuscarDj) {
        this.tfBuscarDj = tfBuscarDj;
    }

    public JTextField getTfCiudadEvento() {
        return tfCiudadEvento;
    }

    public void setTfCiudadEvento(JTextField tfCiudadEvento) {
        this.tfCiudadEvento = tfCiudadEvento;
    }

    public JTextField getTfNumeroEvento() {
        return tfNumeroEvento;
    }

    public void setTfNumeroEvento(JTextField tfNumeroEvento) {
        this.tfNumeroEvento = tfNumeroEvento;
    }

    public JTextField getTfPrecioConstruccionEvento() {
        return tfPrecioConstruccionEvento;
    }

    public void setTfPrecioConstruccionEvento(JTextField tfPrecioConstruccionEvento) {
        this.tfPrecioConstruccionEvento = tfPrecioConstruccionEvento;
    }

    public JTabbedPane getTabbedPane1() {
        return tabbedPane1;
    }

    public void setTabbedPane1(JTabbedPane tabbedPane1) {
        this.tabbedPane1 = tabbedPane1;
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }

    public JTextField getTfNombreDj() {
        return tfNombreDj;
    }

    public void setTfNombreDj(JTextField tfNombreDj) {
        this.tfNombreDj = tfNombreDj;
    }

    public JTextField getTfEdadDj() {
        return tfEdadDj;
    }

    public void setTfEdadDj(JTextField tfEdadDj) {
        this.tfEdadDj = tfEdadDj;
    }

    public JTextField getTfSueldoDj() {
        return tfSueldoDj;
    }

    public void setTfSueldoDj(JTextField tfSueldoDj) {
        this.tfSueldoDj = tfSueldoDj;
    }

    public JButton getBtNuevoDj() {
        return btNuevoDj;
    }

    public void setBtNuevoDj(JButton btNuevoDj) {
        this.btNuevoDj = btNuevoDj;
    }

    public JButton getBtGuardarDj() {
        return btGuardarDj;
    }

    public void setBtGuardarDj(JButton btGuardarDj) {
        this.btGuardarDj = btGuardarDj;
    }

    public JButton getBtModificarDj() {
        return btModificarDj;
    }

    public void setBtModificarDj(JButton btModificarDj) {
        this.btModificarDj = btModificarDj;
    }

    public JButton getBtEliminarDj() {
        return btEliminarDj;
    }

    public void setBtEliminarDj(JButton btEliminarDj) {
        this.btEliminarDj = btEliminarDj;
    }

    public JTextField getTfColorEntrada() {
        return tfColorEntrada;
    }

    public void setTfColorEntrada(JTextField tfColorEntrada) {
        this.tfColorEntrada = tfColorEntrada;
    }

    public JTextField getTfNumeroEntrada() {
        return tfNumeroEntrada;
    }

    public void setTfNumeroEntrada(JTextField tfNumeroEntrada) {
        this.tfNumeroEntrada = tfNumeroEntrada;
    }

    public JTextField getTfPrecioEntrada() {
        return tfPrecioEntrada;
    }

    public void setTfPrecioEntrada(JTextField tfPrecioEntrada) {
        this.tfPrecioEntrada = tfPrecioEntrada;
    }

    public JButton getBtNuevoEntrada() {
        return btNuevoEntrada;
    }

    public void setBtNuevoEntrada(JButton btNuevoEntrada) {
        this.btNuevoEntrada = btNuevoEntrada;
    }

    public JButton getBtGuardarEntrada() {
        return btGuardarEntrada;
    }

    public void setBtGuardarEntrada(JButton btGuardarEntrada) {
        this.btGuardarEntrada = btGuardarEntrada;
    }

    public JButton getBtModificarEntrada() {
        return btModificarEntrada;
    }

    public void setBtModificarEntrada(JButton btModificarEntrada) {
        this.btModificarEntrada = btModificarEntrada;
    }

    public JButton getBtEliminarEntrada() {
        return btEliminarEntrada;
    }

    public void setBtEliminarEntrada(JButton btEliminarEntrada) {
        this.btEliminarEntrada = btEliminarEntrada;
    }

    public JDateChooser getJdcFechaEntrada() {
        return jdcFechaEntrada;
    }

    public void setJdcFechaEntrada(JDateChooser jdcFechaEntrada) {
        this.jdcFechaEntrada = jdcFechaEntrada;
    }

    public JDateChooser getJdcFechaNacimientoDj() {
        return jdcFechaNacimientoDj;
    }

    public void setJdcFechaNacimientoDj(JDateChooser jdcFechaNacimientoDj) {
        this.jdcFechaNacimientoDj = jdcFechaNacimientoDj;
    }

    public JTextField getTfNombreCliente() {
        return tfNombreCliente;
    }

    public void setTfNombreCliente(JTextField tfNombreCliente) {
        this.tfNombreCliente = tfNombreCliente;
    }

    public JTextField getTfEdadCLiente() {
        return tfEdadCLiente;
    }

    public void setTfEdadCLiente(JTextField tfEdadCLiente) {
        this.tfEdadCLiente = tfEdadCLiente;
    }

    public JButton getBtNuevoCliente() {
        return btNuevoCliente;
    }

    public void setBtNuevoCliente(JButton btNuevoCliente) {
        this.btNuevoCliente = btNuevoCliente;
    }

    public JButton getBtGuardarCliente() {
        return btGuardarCliente;
    }

    public void setBtGuardarCliente(JButton btGuardarCliente) {
        this.btGuardarCliente = btGuardarCliente;
    }

    public JButton getBtModificarCliente() {
        return btModificarCliente;
    }

    public void setBtModificarCliente(JButton btModificarCliente) {
        this.btModificarCliente = btModificarCliente;
    }

    public JButton getBtEliminarCliente() {
        return btEliminarCliente;
    }

    public void setBtEliminarCliente(JButton btEliminarCliente) {
        this.btEliminarCliente = btEliminarCliente;
    }

    public JDateChooser getJdcFechaNacimientoCliente() {
        return jdcFechaNacimientoCliente;
    }

    public void setJdcFechaNacimientoCliente(JDateChooser jdcFechaNacimientoCliente) {
        this.jdcFechaNacimientoCliente = jdcFechaNacimientoCliente;
    }

    public JComboBox getCbEventoEntrada() {
        return cbEventoEntrada;
    }

    public void setCbEventoEntrada(JComboBox cbEventoEntrada) {
        this.cbEventoEntrada = cbEventoEntrada;
    }

    public JComboBox getCbEventoDj() {
        return cbEventoDj;
    }

    public void setCbEventoDj(JComboBox cbEventoDj) {
        this.cbEventoDj = cbEventoDj;
    }

    public JComboBox getCbEntradaCliente() {
        return cbEntradaCliente;
    }

    public void setCbEntradaCliente(JComboBox cbEntradaCliente) {
        this.cbEntradaCliente = cbEntradaCliente;
    }

    public JComboBox getCbClienteEntrada() {
        return cbClienteEntrada;
    }

    public void setCbClienteEntrada(JComboBox cbClienteEntrada) {
        this.cbClienteEntrada = cbClienteEntrada;
    }

    public JButton getBtNuevoEvento() {
        return btNuevoEvento;
    }

    public void setBtNuevoEvento(JButton btNuevoEvento) {
        this.btNuevoEvento = btNuevoEvento;
    }

    public JButton getBtGuardarEvento() {
        return btGuardarEvento;
    }

    public void setBtGuardarEvento(JButton btGuardarEvento) {
        this.btGuardarEvento = btGuardarEvento;
    }

    public JButton getBtModificarEvento() {
        return btModificarEvento;
    }

    public void setBtModificarEvento(JButton btModificarEvento) {
        this.btModificarEvento = btModificarEvento;
    }

    public JButton getBtEliminarEvento() {
        return btEliminarEvento;
    }

    public void setBtEliminarEvento(JButton btEliminarEvento) {
        this.btEliminarEvento = btEliminarEvento;
    }

    public JTextField getTfNombreEvento() {
        return tfNombreEvento;
    }

    public void setTfNombreEvento(JTextField tfNombreEvento) {
        this.tfNombreEvento = tfNombreEvento;
    }

    public JComboBox getCbClienteEvento() {
        return cbClienteEvento;
    }

    public void setCbClienteEvento(JComboBox cbClienteEvento) {
        this.cbClienteEvento = cbClienteEvento;
    }

    public JComboBox getCbDjEvento() {
        return cbDjEvento;
    }

    public void setCbDjEvento(JComboBox cbDjEvento) {
        this.cbDjEvento = cbDjEvento;
    }

    public JList getlEvento() {
        return lEvento;
    }

    public void setlEvento(JList lEvento) {
        this.lEvento = lEvento;
    }

    public JList getlCliente() {
        return lCliente;
    }

    public void setlCliente(JList lCliente) {
        this.lCliente = lCliente;
    }

    public JList getlEntrada() {
        return lEntrada;
    }

    public void setlEntrada(JList lEntrada) {
        this.lEntrada = lEntrada;
    }

    public JList getlDj() {
        return lDj;
    }

    public void setlDj(JList lDj) {
        this.lDj = lDj;
    }

    public DefaultListModel getModeloListaCliente() {
        return modeloListaCliente;
    }

    public void setModeloListaCliente(DefaultListModel modeloListaCliente) {
        this.modeloListaCliente = modeloListaCliente;
    }

    public DefaultListModel getModeloListaEvento() {
        return modeloListaEvento;
    }

    public void setModeloListaEvento(DefaultListModel modeloListaEvento) {
        this.modeloListaEvento = modeloListaEvento;
    }

    public DefaultListModel getModeloListaEntrada() {
        return modeloListaEntrada;
    }

    public void setModeloListaEntrada(DefaultListModel modeloListaEntrada) {
        this.modeloListaEntrada = modeloListaEntrada;
    }

    public DefaultListModel getModeloListaDj() {
        return modeloListaDj;
    }

    public void setModeloListaDj(DefaultListModel modeloListaDj) {
        this.modeloListaDj = modeloListaDj;
    }
}
