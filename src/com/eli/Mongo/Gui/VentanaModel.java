package com.eli.Mongo.Gui;

import com.eli.Mongo.Base.Cliente;
import com.eli.Mongo.Base.DJ;
import com.eli.Mongo.Base.Entrada;
import com.eli.Mongo.Base.Evento;
import com.eli.Mongo.Window.Ventana;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Eliseo on 05/03/2016.
 */
public class VentanaModel {

    private DJ dj;
    private Evento evento;
    private Entrada entrada;
    private Cliente cliente;
    private Ventana ventana;
    public List<Cliente> listaClientes;


    MongoClient mongoCliente;
    MongoDatabase mongoDatabase;
    public void conectar(){
        mongoCliente =new MongoClient();
        mongoDatabase= mongoCliente.getDatabase("Djs");
    }

    public void guardarClienteMongo(Cliente cliente){
        Document document=new Document()
                .append("nombre",cliente.getNombre())
                .append("edad",cliente.getEdad())
                .append("fecha nacimiento",cliente.getFecha_nacimiento());
        mongoDatabase.getCollection("Cliente").insertOne(document);
    }

    public void eliminarClienteMongo(String nombre){
        mongoDatabase.getCollection(Cliente.COLECCION).deleteOne(new Document("nombre", nombre));
    }

    public void modificarClienteMongo(Cliente cliente){
        mongoDatabase.getCollection(Cliente.COLECCION).replaceOne(new Document("id", cliente.getId()), new Document()
                .append("nombre", cliente.getNombre())
                .append("edad", cliente.getEdad())
                .append("fecha nacimiento", cliente.getFecha_nacimiento()));
    }



    public void guardarDjMongo(DJ dj){
        Document documento=new Document()
        .append("nombre", dj.getNombre())
        .append("edad", dj.getEdad())
        .append("sueldo", dj.getSueldo())
        .append("fecha nacimiento", dj.getFecha_nacimiento_dj());
        mongoDatabase.getCollection("DJ").insertOne(documento);
    }

    public void eliminarDjMongo(String nombre){
        mongoDatabase.getCollection(DJ.COLECCION).deleteOne(new Document("nombre", nombre));
    }

    public void modificarDjMongo(DJ dj){
        mongoDatabase.getCollection(DJ.COLECCION).replaceOne(new Document("id", dj.getId()), new Document()
                .append("nombre", dj.getNombre())
                .append("edad", dj.getEdad())
                .append("sueldo", dj.getSueldo())
                .append("fecha nacimiento", dj.getFecha_nacimiento_dj()));
    }


    public void guardarEntradaMongo(Entrada entrada){
        Document document=new Document()
                .append("color",entrada.getColor())
                .append("numero",entrada.getNumero())
                .append("precio",entrada.getPrecio())
                .append("fecha",entrada.getFecha());
        mongoDatabase.getCollection("Entrada").insertOne(document);
    }

    public void eliminarEntradaMongo(String color){
        mongoDatabase.getCollection(Entrada.COLECCION).deleteOne(new Document("color", color));
    }

    public void modificarEntradaMongo(Entrada entrada){
        mongoDatabase.getCollection(Entrada.COLECCION).replaceOne(new Document("id", entrada.getId()), new Document()
                .append("color", entrada.getColor())
                .append("numero", entrada.getNumero())
                .append("precio", entrada.getPrecio())
                .append("fecha", entrada.getFecha()));
    }


    public void guardarEventoMongo(Evento evento){
        Document document=new Document()
                .append("nombre",evento.getNombre())
                .append("ciudad",evento.getCiudad())
                .append("numero",evento.getNumero())
                .append("precio",evento.getPrecio_construccion())
                .append("fecha",evento.getFecha());
        mongoDatabase.getCollection("Evento").insertOne(document);
    }

    public void eliminarEventoMongo(String nombre){
        mongoDatabase.getCollection(Evento.COLECCION).deleteOne(new Document("nombre", nombre));
    }

    public void modificarEventoMongo(Evento evento){
        mongoDatabase.getCollection(Entrada.COLECCION).replaceOne(new Document("id", evento.getId()), new Document()
                .append("nombre", evento.getNombre())
                .append("ciudad", evento.getCiudad())
                .append("numero", evento.getNumero())
                .append("precio", evento.getPrecio_construccion())
                .append("fecha", evento.getFecha()));
    }




    public List<Evento> getListaEventos(FindIterable<Document> findIterable) {
        List<Evento> listaEventos = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        while (iterator.hasNext()){
            Document document = iterator.next();
            evento = new Evento();
            evento.setId(document.getObjectId("id"));
            evento.setNombre(document.getString("nombre"));
            evento.setCiudad(document.getString("ciudad"));
            evento.setNumero(document.getInteger("numero"));
            evento.setPrecio_construccion(document.getInteger("precio"));

            listaEventos.add(evento);
        }
        return listaEventos;

    }

    public List<Cliente> getListaClientes(FindIterable<Document> findIterable) {
        listaClientes = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        while (iterator.hasNext()){
            Document document = iterator.next();
            cliente=new Cliente();
            cliente.setId(document.getObjectId("id"));
            cliente.setNombre(document.getString("nombre"));
            cliente.setEdad(document.getInteger("edad"));
            cliente.setFecha_nacimiento(document.getDate("fecha"));

            listaClientes.add(cliente);
        }
        return listaClientes;

    }

    public List<Entrada> getListaEntradas(FindIterable<Document> findIterable) {
        List<Entrada> listaEntradas = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        while (iterator.hasNext()){
            Document document = iterator.next();
            entrada=new Entrada();
            entrada.setId(document.getObjectId("id"));
            entrada.setPrecio(document.getInteger("precio"));
            entrada.setNumero(document.getInteger("numero"));
            entrada.setColor(document.getString("color"));
            entrada.setFecha(document.getDate("fecha"));

            listaEntradas.add(entrada);
        }
        return listaEntradas;

    }

    public List<DJ> getListaDJ(FindIterable<Document> findIterable) {
        List<DJ> listaDJs = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        while (iterator.hasNext()){
            Document document = iterator.next();
            dj=new DJ();
            dj.setId(document.getObjectId("id"));
            dj.setNombre(document.getString("nombre"));
            dj.setSueldo(document.getInteger("sueldo"));
            dj.setEdad(document.getInteger("edad"));
            dj.setFecha_nacimiento_dj(document.getDate("fecha"));

            listaDJs.add(dj);
        }
        return listaDJs;

    }

    public List<DJ> buscarDj(String busqueda) throws ParseException {

        Document documento = new Document("$or", Arrays.asList(
                new Document("nombre", busqueda),
                new Document("edad", busqueda),
                new Document("sueldo", busqueda)));

        FindIterable findIterable = mongoDatabase.getCollection(DJ.COLECCION)
                .find(documento)
                .sort(new Document("nombre", 1));
        return getListaDJ(findIterable);
    }







    public List<DJ> getDJ()throws ParseException{
        FindIterable<Document> findIterable=mongoDatabase.getCollection(DJ.COLECCION).find();
        return getListaDJ(findIterable);
    }

    public List<Evento> getEvento()throws ParseException{
        FindIterable<Document> findIterable=mongoDatabase.getCollection(Evento.COLECCION).find();
        return getListaEventos(findIterable);
    }

    public List<Entrada> getEntrada()throws ParseException{
        FindIterable<Document> findIterable=mongoDatabase.getCollection(Entrada.COLECCION).find();
        return getListaEntradas(findIterable);
    }

    public List<Cliente> getCliente()throws ParseException{
        FindIterable<Document> findIterable=mongoDatabase.getCollection(Cliente.COLECCION).find();
        return getListaClientes(findIterable);
    }




}
