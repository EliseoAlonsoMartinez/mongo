package com.eli.Mongo.Gui;

import com.eli.Mongo.Base.Cliente;
import com.eli.Mongo.Base.DJ;
import com.eli.Mongo.Base.Entrada;
import com.eli.Mongo.Base.Evento;
import com.eli.Mongo.Window.Ventana;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eliseo on 05/03/2016.
 */
public class VentanaController {
    private Cliente cliente;
    private DJ dj;
    private Entrada entrada;
    private Evento evento;

    private Cliente clienteSeleccionado;
    private DJ djSeleccionado;
    private Entrada entradaSeleccionada;
    private Evento eventoSeleccionado;

    private Ventana ventana;
    private VentanaModel ventanaModel;

    private boolean nuevoDj=false;
    private boolean nuevaEntrada=false;
    private boolean nuevoEvento=false;
    private boolean nuevoCliente=false;

    public VentanaController(Ventana ventana,VentanaModel ventanaModel){

        this.ventana=ventana;
        this.ventanaModel=ventanaModel;


        ventanaModel.conectar();

        initListas();
        listarClientes();
        listarDJS();
        listarEntradas();

        listarEventos();
        rellenarComboEventoDj();
        rellenarCombosEntradaClinente();
        rellenarComboDjEvento();
        rellenarComboEventoEntrada();


        ventana.lDj.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                djSeleccionado = (DJ) ventana.lDj.getSelectedValue();
                cargarDj();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        ventana.lEntrada.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                entradaSeleccionada=(Entrada) ventana.lEntrada.getSelectedValue();
                cargarEntrada();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        ventana.lCliente.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                clienteSeleccionado=(Cliente) ventana.lCliente.getSelectedValue();
                cargarCliente();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        ventana.lEvento.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                eventoSeleccionado=(Evento) ventana.lEvento.getSelectedValue();
                cargarEvento();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        ventana.btNuevoDj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoDj = true;
                ventana.getTfNombreDj().setEnabled(true);
                ventana.getTfEdadDj().setEnabled(true);
                ventana.getTfSueldoDj().setEnabled(true);
                ventana.getCbEventoDj().setEnabled(true);
                listarDJS();
            }
        });
        ventana.btGuardarDj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (nuevoDj == true) {
                    dj = new DJ();
                } else {
                    dj = djSeleccionado;
                }

                dj = new DJ();
                dj.setNombre(ventana.getTfNombreDj().getText());
                dj.setEdad(Integer.parseInt(ventana.getTfEdadDj().getText()));
                dj.setSueldo(Integer.parseInt(ventana.getTfSueldoDj().getText()));
                dj.setFecha_nacimiento_dj(ventana.getJdcFechaNacimientoDj().getDate());
                dj.setEvento((Evento) ventana.getCbEventoDj().getSelectedItem());


                if (nuevoDj) {
                    ventanaModel.guardarDjMongo(dj);
                } else {
                    ventanaModel.modificarDjMongo(dj);
                }

                ventana.getTfNombreDj().setEnabled(false);
                ventana.getTfEdadDj().setEnabled(false);
                ventana.getTfSueldoDj().setEnabled(false);
                ventana.getCbEventoDj().setEnabled(false);
                listarDJS();
                rellenarComboDjEvento();
                nuevoDj = false;

            }
        });
        ventana.btEliminarDj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dj=(DJ)ventana.lDj.getSelectedValue();
                ventanaModel.eliminarDjMongo(dj.getNombre());
                listarDJS();
            }
        });
        ventana.btModificarDj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ventana.getTfNombreDj().setEnabled(true);
                ventana.getTfEdadDj().setEnabled(true);
                ventana.getTfSueldoDj().setEnabled(true);
                ventana.getCbEventoDj().setEnabled(true);
            }
        });
        ventana.btBuscarDj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buscarDj(ventana.btBuscarDj.getText());
                if (ventana.btBuscarDj.getText().equals("")) {
                    listarDJS();
                }
            }
        });

        ventana.btNuevoEntrada.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevaEntrada = true;
                ventana.getTfColorEntrada().setEnabled(true);
                ventana.getTfNumeroEntrada().setEnabled(true);
                ventana.getTfPrecioEntrada().setEnabled(true);
                ventana.getCbEventoEntrada().setEnabled(true);
                ventana.getCbClienteEntrada().setEnabled(true);
            }
        });
        ventana.btGuardarEntrada.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (nuevaEntrada) {
                    entrada = new Entrada();

                } else {
                    entrada = entradaSeleccionada;
                }


                entrada = new Entrada();
                entrada.setNumero(Integer.parseInt(ventana.getTfNumeroEntrada().getText()));
                entrada.setColor(ventana.getTfColorEntrada().getText());
                entrada.setPrecio(Integer.parseInt(ventana.getTfPrecioEntrada().getText()));
                entrada.setFecha(ventana.getJdcFechaEntrada().getDate());
                entrada.setCliente((Cliente) ventana.getCbClienteEntrada().getSelectedItem());
                entrada.setEvento((Evento) ventana.getCbEventoEntrada().getSelectedItem());

                if (nuevaEntrada) {
                    ventanaModel.guardarEntradaMongo(entrada);
                } else {
                    ventanaModel.modificarEntradaMongo(entrada);
                }

                ventana.getTfColorEntrada().setEnabled(false);
                ventana.getTfNumeroEntrada().setEnabled(false);
                ventana.getTfPrecioEntrada().setEnabled(false);
                ventana.getCbEventoEntrada().setEnabled(false);
                ventana.getCbClienteEntrada().setEnabled(false);

                rellenarCombosEntradaClinente();
                rellenarComboEventoEntrada();
                listarEntradas();
                nuevaEntrada = false;

            }
        });
        ventana.btEliminarEntrada.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                entrada=(Entrada) ventana.lEntrada.getSelectedValue();
                ventanaModel.eliminarEntradaMongo(entrada.getColor());
                listarEntradas();
            }
        });
        ventana.btModificarEntrada.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoDj=false;
                ventana.getTfNumeroEntrada().setEnabled(true);
                ventana.getTfPrecioEntrada().setEnabled(true);
                ventana.getTfColorEntrada().setEnabled(true);
                ventana.getCbEventoEntrada().setEnabled(true);
                ventana.getCbClienteEntrada().setEnabled(true);
            }
        });

        ventana.btNuevoEvento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoEvento=true;
                ventana.getTfNombreEvento().setEnabled(true);
                ventana.getTfCiudadEvento().setEnabled(true);
                ventana.getTfNumeroEvento().setEnabled(true);
                ventana.getTfPrecioConstruccionEvento().setEnabled(true);
                ventana.getCbDjEvento().setEnabled(true);
                ventana.getCbClienteEvento().setEnabled(true);

            }
        });
        ventana.btGuardarEvento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(nuevoEvento){
                    evento=new Evento();
                }else{
                    evento=eventoSeleccionado;
                }

                evento=new Evento();
                evento.setNombre(ventana.getTfNombreEvento().getText());
                evento.setPrecio_construccion(Integer.parseInt(ventana.getTfPrecioConstruccionEvento().getText()));
                evento.setNumero(Integer.parseInt(ventana.getTfNumeroEvento().getText()));
                evento.setCiudad(ventana.getTfCiudadEvento().getText());
                evento.setDj((DJ) ventana.getCbDjEvento().getSelectedItem());
                evento.setCliente((Cliente) ventana.getCbClienteEvento().getSelectedItem());

                if(nuevoEvento){
                    ventanaModel.guardarEventoMongo(evento);
                }else {
                    ventanaModel.modificarEventoMongo(evento);
                }

                ventana.getTfNombreEvento().setEnabled(false);
                ventana.getTfCiudadEvento().setEnabled(false);
                ventana.getTfNumeroEvento().setEnabled(false);
                ventana.getTfPrecioConstruccionEvento().setEnabled(false);
                ventana.getCbDjEvento().setEnabled(false);
                ventana.getCbClienteEvento().setEnabled(false);

                nuevoEvento=false;
                listarEventos();
                rellenarComboEventoEntrada();
                rellenarComboEventoDj();

            }
        });
        ventana.btEliminarEvento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                evento=(Evento) ventana.lEvento.getSelectedValue();
                ventanaModel.eliminarEventoMongo(evento.getNombre());
                listarEventos();
            }
        });
        ventana.btModificarEvento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoEvento=false;
                ventana.getTfNombreEvento().setEnabled(true);
                ventana.getTfCiudadEvento().setEnabled(true);
                ventana.getTfNumeroEvento().setEnabled(true);
                ventana.getTfPrecioConstruccionEvento().setEnabled(true);
                ventana.getCbDjEvento().setEnabled(true);
                ventana.getCbClienteEvento().setEnabled(true);
            }
        });

        ventana.btNuevoCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoCliente=true;
                ventana.getTfNombreCliente().setEnabled(true);
                ventana.getTfEdadCLiente().setEnabled(true);
                ventana.getJdcFechaNacimientoCliente().setEnabled(true);
                ventana.getCbEntradaCliente().setEnabled(true);
            }
        });
        ventana.btGuardarCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(nuevoCliente){
                    cliente=new Cliente();
                }else{
                    cliente=clienteSeleccionado;
                }
                cliente=new Cliente();
                cliente.setEdad(Integer.parseInt(ventana.getTfEdadCLiente().getText()));
                cliente.setNombre(ventana.tfNombreCliente.getText());
                cliente.setFecha_nacimiento(ventana.jdcFechaNacimientoCliente.getDate());
                cliente.setEntrada((Entrada) ventana.cbEntradaCliente.getSelectedItem());

                if(nuevoCliente){
                    ventanaModel.guardarClienteMongo(cliente);
                }else{
                    ventanaModel.modificarClienteMongo(cliente);
                }
                ventana.getTfNombreCliente().setEnabled(false);
                ventana.getTfEdadCLiente().setEnabled(false);
                ventana.getJdcFechaNacimientoCliente().setEnabled(false);
                ventana.getCbEntradaCliente().setEnabled(false);

                rellenarCombosEntradaClinente();
                rellenarComboClienteEvento();
                rellenarComboClienteEntrada();
                listarClientes();

                nuevoCliente=false;
            }
        });
        ventana.btModificarCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoCliente=false;
                ventana.getTfNombreCliente().setEnabled(true);
                ventana.getTfEdadCLiente().setEnabled(true);
                ventana.getJdcFechaNacimientoCliente().setEnabled(true);
                ventana.getCbEntradaCliente().setEnabled(true);

            }
        });
        ventana.btEliminarCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cliente=(Cliente) ventana.lCliente.getSelectedValue();
                ventanaModel.eliminarClienteMongo(cliente.getNombre());
                listarClientes();
            }
        });
    }

    public void initListas(){
        ventana.lDj.setModel(ventana.getModeloListaDj());
        ventana.lCliente.setModel(ventana.getModeloListaCliente());
        ventana.lEvento.setModel(ventana.getModeloListaEvento());
        ventana.lEntrada.setModel(ventana.getModeloListaEntrada());
    }


    public void listarDJS(){
        try {
            List<DJ> listaDJS=ventanaModel.getDJ();
            ventana.getModeloListaDj().removeAllElements();
            for(DJ dj:listaDJS){
                ventana.getModeloListaDj().addElement(dj);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public void listarEventos(){

        try {
            List<Evento>listaEventos = ventanaModel.getEvento();
            ventana.getModeloListaEvento().removeAllElements();
            for (Evento evento : listaEventos) {
                ventana.getModeloListaEvento().addElement(evento);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }
    public void listarEntradas(){
        try {
            List<Entrada> listaEntradas = ventanaModel.getEntrada();
            ventana.getModeloListaEntrada().removeAllElements();
            for(Entrada entrada:listaEntradas){
                ventana.getModeloListaEntrada().addElement(entrada);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void listarClientes(){
        try {
            List<Cliente> listaClientes= ventanaModel.getCliente();
            ventana.getModeloListaCliente().removeAllElements();
            for(Cliente cliente:listaClientes){
                ventana.getModeloListaCliente().addElement(cliente);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }




    public void rellenarComboEventoDj(){
        try {
            List<Evento>listaEventos = ventanaModel.getEvento();
            ventana.cbEventoDj.removeAllItems();
            for(Evento evento:listaEventos){
                ventana.cbEventoDj.addItem(evento);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    public void rellenarCombosEntradaClinente(){
        try {
            List<Entrada> listaEntradas=ventanaModel.getEntrada();
            ventana.getCbEntradaCliente().removeAllItems();
            for(Entrada entrada:listaEntradas){
                ventana.getCbEntradaCliente().addItem(entrada);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void rellenarComboClienteEntrada(){
        try {
            List<Cliente> listaClientes=ventanaModel.getCliente();
            ventana.getCbClienteEntrada().removeAllItems();
            for(Cliente cliente:listaClientes){
                ventana.getCbClienteEntrada().addItem(cliente);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void rellenarComboEventoEntrada(){
        try {
            List<Evento> listaEventos=ventanaModel.getEvento();
            ventana.cbEventoEntrada.removeAllItems();
            for(Evento evento:listaEventos){
                ventana.cbEventoEntrada.addItem(evento);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public void rellenarComboDjEvento(){
        try {
            List<DJ> listaDjs=ventanaModel.getDJ();
            ventana.cbDjEvento.removeAllItems();
            for(DJ dj:listaDjs){
                ventana.cbDjEvento.addItem(dj);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void rellenarComboClienteEvento(){
        try {
            List<Cliente> listaClientes = ventanaModel.getCliente();
            ventana.getCbClienteEvento().removeAllItems();
            for(Cliente cliente:listaClientes){
                ventana.getCbClienteEvento().addItem(cliente);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void cargarDj(){
        ventana.tfNombreDj.setText(djSeleccionado.getNombre());
        ventana.tfEdadDj.setText(String.valueOf(djSeleccionado.getEdad()));
        ventana.tfSueldoDj.setText(String.valueOf(djSeleccionado.getSueldo()));
        ventana.cbEventoDj.setToolTipText(String.valueOf(djSeleccionado.getEvento()));
        ventana.jdcFechaNacimientoDj.setDate(djSeleccionado.getFecha_nacimiento_dj());
    }

    public void cargarEntrada(){
        ventana.tfColorEntrada.setText(entradaSeleccionada.getColor());
        ventana.tfNumeroEntrada.setText(String.valueOf(entradaSeleccionada.getNumero()));
        ventana.tfPrecioEntrada.setText(String.valueOf(entradaSeleccionada.getPrecio()));
        ventana.cbClienteEntrada.setToolTipText(String.valueOf(entradaSeleccionada.getCliente()));
        ventana.cbEventoEntrada.setToolTipText(String.valueOf(entradaSeleccionada.getEvento()));
        ventana.jdcFechaEntrada.setDate(entradaSeleccionada.getFecha());
    }

    public void cargarCliente(){
        ventana.tfNombreCliente.setText(clienteSeleccionado.getNombre());
        ventana.tfEdadCLiente.setText(String.valueOf(clienteSeleccionado.getEdad()));
        ventana.jdcFechaNacimientoCliente.setDate(clienteSeleccionado.getFecha_nacimiento());
        ventana.cbEntradaCliente.setToolTipText(String.valueOf(clienteSeleccionado.getEntrada()));
    }

    public void cargarEvento(){
        ventana.tfNombreEvento.setText(eventoSeleccionado.getNombre());
        ventana.tfCiudadEvento.setText(eventoSeleccionado.getCiudad());
        ventana.tfNumeroEvento.setText(String.valueOf(eventoSeleccionado.getNumero()));
        ventana.cbClienteEvento.setToolTipText(String.valueOf(eventoSeleccionado.getCliente()));
        ventana.cbDjEvento.setToolTipText(String.valueOf(eventoSeleccionado.getDj()));
        ventana.tfPrecioConstruccionEvento.setText(String.valueOf(eventoSeleccionado.getPrecio_construccion()));
    }

    private  void buscarDj(String dato)  {
        List<DJ> busquedaDj = null;
        try {
            busquedaDj = ventanaModel.getDJ();

            ventana.getModeloListaDj().removeAllElements();
            List<DJ> listaDJS = new ArrayList<>();
            try {
                listaDJS = ventanaModel.buscarDj(ventana.tfBuscarDj.getText());
            } catch (ParseException pe) {
            }
            ventana.getModeloListaDj().removeAllElements();
            for (DJ dj : listaDJS){
                ventana.getModeloListaDj().addElement(dj);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }



}
